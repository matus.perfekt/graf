from src.Node import Node
import math
import random
import pygame
from src import Statics, Vector
import json


class Graph:
    def __init__(self):
        self.nodes = []
        self.spring_length = 30
        self.force_layout = False
        self.moving = None
        self.edge_drawing = [None, None]

    def toggle_force_layout(self):
        self.force_layout = not self.force_layout

    def generate_graph(self, node_count):
        for i in range(node_count):
            self.nodes.append(Node(i))

        if node_count > 1:
            for node in self.nodes:
                while node.get_edge_count() < (1 if node_count == 2 else random.randrange(1, 3)):
                    second_node = random.choice([n for n in self.nodes if (n != node and not n.connected_to(node))])
                    self.add_edge(node, second_node)

    def color_differ(self, recolor=False):
        visited = []
        for node in self.nodes:
            if node not in visited:
                if not recolor:
                    color = node.get_color()
                else:
                    color = (random.randrange(10, 200), random.randrange(10, 200), random.randrange(10, 200))

                self.color_node(node, color, visited)

    def color_node(self, node, color, vis):
        vis.append(node)
        node.set_col(color)
        for neigh in node.get_neighbors():
            if neigh not in vis:
                self.color_node(neigh, color, vis)

    def calculate_circular_positions(self):
        """Sets position of all nodes to shape circle"""
        dst = (2 * math.pi) / len(self.nodes)
        radius = len(self.nodes) * 10
        angle = 0
        width, height = pygame.display.get_surface().get_size()
        for node in self.nodes:
            node.set_pos(
                width / 2 + radius * math.cos(angle),
                height / 2 + radius * math.sin(angle)
            )
            angle += dst

    def add_node(self, new_node):
        self.nodes.append(new_node)

    def add_edge(self, node1, node2):
        node1.connect(node2)
        node2.connect(node1)

    def remove_node(self, node):
        for edge in node.get_edges():
            edge.remove_edge(node)
        self.nodes.remove(node)

    def remove_edge(self, node1, node2):
        node1.remove_edge(node2)
        node2.remove_edge(node1)

    def calculate_repulsion(self, n1, n2):
        # prevent zero division
        dst = max(Vector.dst(n1.get_pos(), n2.get_pos()), 0.001)
        k = 400
        direction = Vector.subtract(n1.get_pos(), n2.get_pos()).normalized()
        direction.multiply(k / (dst ** 2))
        return direction

    def calculate_attraction(self, n1, n2):
        dst = max(Vector.dst(n1.get_pos(), n2.get_pos()), 0.001)
        direction = Vector.subtract(n2.get_pos(), n1.get_pos()).normalized()
        direction.multiply(5*math.log10(dst/self.spring_length))
        return direction

    def calculate_force(self, node):
        force = Vector.Vector2(0, 0)
        for n in [n for n in self.nodes if n != node]:
            force.add(self.calculate_repulsion(node, n))
        for n in node.get_neighbors():
            force.add(self.calculate_attraction(node, n))
        return force

    def update(self, delta):
        if self.force_layout:
            for node in self.nodes:
                node.apply_force(self.calculate_force(node))
                node.update(delta)

        if self.moving:
            self.nodes[self.moving].set_pos(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])

    def draw(self, screen):
        for node in self.nodes:
            node.draw_edges(screen)

        for node in self.nodes:
            node.draw(screen)

    def is_on_node(self, x, y):
        for i in range(len(self.nodes)):
            if Vector.dst(self.nodes[i].get_pos(), Vector.Vector2(x, y)) < self.nodes[i].get_size():
                return i
        return None

    def move_node(self, mouse_pos):
        """Register index of the node, the user is moving with mouse"""
        self.moving = self.is_on_node(mouse_pos[0], mouse_pos[1])

    def drop_node(self):
        self.moving = None

    def right_click(self):
        cur = self.is_on_node(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
        if cur:
            if self.edge_drawing[0]:
                self.add_edge(self.nodes[self.edge_drawing[0]], self.nodes[cur])
                self.color_differ()
                self.edge_drawing = [None, None]
            else:
                self.edge_drawing = [cur, None]
            return

        self.add_node(Node(len(self.nodes), Vector.Vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])))
        self.color_differ()

    def save(self):
        ind = 0
        f = None
        while f is None:
            if Statics.exists("graph{0}.json".format(ind)):
                ind += 1
            else:
                f = open("graph{0}.json".format(ind), "w+")
        json_dict = {}
        for i in range(len(self.nodes)):
            json_dict[i] = {
                "data": self.nodes[i].get_data(),
                "color": self.nodes[i].get_color(),
                "edges": self.nodes[i].get_edge_data()
            }

        f.write(json.dumps(json_dict, separators=(', ', ': '), indent=4))
        f.close()

    def load(self, name):
        """Enter name with .json"""

        self.nodes.clear()

        f = open(name)
        json_dict = json.load(f)
        for n in json_dict.values():
            new_node = Node(n['data'])
            new_node.set_col(n['color'])
            self.nodes.append(new_node)

        # current pojde cez kazdy node a edges bude array susedov pre ten node loaded zo suboru
        for current, edges in zip(self.nodes, [d['edges'] for d in json_dict.values()]):
            for edge in edges:
                self.add_edge(current, self.get_node_by_data(edge))

        f.close()

    def shortest_path(self, start_node, end_node):
        queue = [start_node]
        score = [0]

        current = 0
        while len(queue) != len(self.nodes):
            if len(queue)-1 < current:
                return -1
            for neigh in [neigh for neigh in queue[current].get_neighbors() if neigh not in queue]:
                if neigh == end_node:
                    return score[current] + 1
                queue.append(neigh)
                score.append(score[current] + 1)
            current += 1

        return -1

    def get_node(self, index):
        return self.nodes[index]

    def get_node_by_data(self, data):
        for node in self.nodes:
            if str(node.get_data()) == str(data):
                return node
        return None
