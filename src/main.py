import pygame
import sys
from pygame.locals import *

from src.Graph import Graph

# TODO cannot move first node

pygame.init()
DISPLAY = pygame.display.set_mode((1000, 1000))
pygame.display.set_caption("Graph")
clock = pygame.time.Clock()

graph = Graph()
# graph.generate_graph(10)
graph.color_differ(True)
# graph.save()
graph.load("demo_graph.json")
#print(graph.shortest_path(graph.get_node(0), graph.get_node(9)))


def update(delta):
    graph.update(delta)


def render():
    graph.draw(DISPLAY)


while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN:
            graph.toggle_force_layout()

        if event.type == MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
            graph.move_node(pygame.mouse.get_pos())
        elif event.type == MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[2]:
            graph.right_click()

        if event.type == MOUSEBUTTONUP:
            graph.drop_node()
    DISPLAY.fill((125, 125, 125))
    d = clock.tick_busy_loop(60)/25
    update(d)
    render()
    pygame.display.update()
