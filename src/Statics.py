def exists(file_name):
    try:
        file = open(file_name, 'r')
        file.close()
    except IOError:
        return False
    return True